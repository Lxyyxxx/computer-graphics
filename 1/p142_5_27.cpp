//
// Created by Xinyu on 2019/10/16 18:03.
// p142 5.27
// 试用OpenGL程序绘制实线、虚线和点划线
//

#include <GL/glut.h>

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0.0, 100.0, 0.0, 100.0);
}

void Lines(){
    // 实线
    glColor3f(0.5f, 0.0f, 0.0f); // red
    glBegin(GL_LINES);
    glVertex2i(20, 20);
    glVertex2i(80, 20);
    glEnd();
    // 虚线
    glColor3f(0.0f, 0.0f, 0.5f); // blue
    glLineStipple(5, 0x5555); // 0x5555:0101010101010101
    glEnable(GL_LINE_STIPPLE); // 允许glLineStipple
    glBegin(GL_LINES);
    glVertex2i(20, 50);
    glVertex2i(80, 50);
    glEnd();
    glDisable(GL_LINE_STIPPLE); // 关闭glLineStipple
    // 点划线
    glColor3f(0.0f, 0.0f, 0.0f); // black
    glLineStipple(1, 0x8FF1); // 0x8FF1:1000111111110001
    glEnable(GL_LINE_STIPPLE); // 允许glLineStipple
    glBegin(GL_LINES);
    glVertex2i(20, 80);
    glVertex2i(80, 80);
    glEnd();
    glDisable(GL_LINE_STIPPLE); // 关闭glLineStipple
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    Lines(); // 画线
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Line");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}
