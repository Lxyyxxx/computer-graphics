//
// Created by Xinyu on 2019/10/16 21:18.
// p12 2
// 创建一个球体动画，使球体在窗口内做自由落体运动，并在撞击地面后能够弹回原来的高度。
//

#include <GL/glut.h>
#include <cmath>

// 窗体的高度和宽度
const float height=5.5f, width=5.5f;
// 球的半径
const float r=0.5f;
// 当前y坐标, 上一y坐标
float ynow=0.0f, ylast=0.0f;
// 自由落体=1，反弹=0
bool flag=true;
// 重力加速度，为了效果缩小10倍
const float g=0.98f;
// 计数器，当作时间用于算位置
int cnt=0;

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(-width, width, -height, height);
}

void Timer(int value){
    cnt++;
    // 记录上一坐标
    ylast=ynow;
    // 更新当前坐标
    if(flag) ynow=-0.5f*g*cnt*cnt;
    else ynow=-height+r+sqrt(2*g*(height-r))*cnt-0.5*g*cnt*cnt;
    // 触底反弹
    if(ynow<-height+r){
        flag=false;
        cnt=0;
        ynow=-height+r;
    }
    // 自由落体
    else if(ynow>=0){
        flag=true;
        cnt=0;
        ynow=0.0;
    }
    glutPostRedisplay(); // 重画，配合计时器产生动画
    glutTimerFunc(90, Timer, 1);
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    glTranslatef(0.0f, ynow-ylast, 0.0f); // 改变当前坐标中心，产生动画效果
    glColor3f(0.7f, 0.8f, 0.8f);
    glutSolidSphere (r, 30, 30); // 实心球
    glColor3f(0.0f, 0.0f, 0.5f);
    glutWireSphere(r, 30, 30); // 线框球
    glutSwapBuffers();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Pinball");
    glutDisplayFunc(Display);
    glutTimerFunc(90, Timer, 1);
    Initial();
    glutMainLoop();
    return 0;
}
