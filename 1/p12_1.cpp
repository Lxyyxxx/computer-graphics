//
// Created by Xinyu on 2019/10/16 20:32.
// p12 1
// 在窗口的中心绘制一个球体
//

#include <GL/glut.h>

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(-1.5f, 1.5f, -1.5f, 1.5f);
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.7f, 0.8f, 0.8f);
    glutSolidSphere (1.0, 50, 50); // 实心球
    glColor3f(0.0f, 0.0f, 0.5f);
    glutWireSphere (1.0, 50, 50); // 线框球
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Ball");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}
