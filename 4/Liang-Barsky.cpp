//
// Created by Xinyu on 2019/11/14 10:36.
//

#include <gl/glut.h>

const float xwl=0.0f, xwr=2.0f, ywb=0.0f, ywt=2.0f;

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(-5.0, 5.0, -5.0, 5.0);
}

void drawxy(){
    /*
     * 画坐标轴和窗口
     */
    glColor3f(0.7, 0.7, 0); // yellow
    // x
    glBegin(GL_LINES);
    glVertex2f(-5, 0);
    glVertex2f(5, 0);
    glEnd();
    // y
    glBegin(GL_LINES);
    glVertex2f(0, -5);
    glVertex2f(0, 5);
    glEnd();

    glColor3f(0, 0.7, 0.7); // green
    // left
    glBegin(GL_LINES);
    glVertex2f(xwl, ywb);
    glVertex2f(xwl, ywt);
    glEnd();
    // right
    glBegin(GL_LINES);
    glVertex2f(xwr, ywb);
    glVertex2f(xwr, ywt);
    glEnd();
    // top
    glBegin(GL_LINES);
    glVertex2f(xwr, ywt);
    glVertex2f(xwl, ywt);
    glEnd();
    // bottom
    glBegin(GL_LINES);
    glVertex2f(xwr, ywb);
    glVertex2f(xwl, ywb);
    glEnd();
}

bool run(float p, float q, float &umax, float &umin){
    /*
     * 处理判断直线与窗口边界关系
     */
    float r;
    if(p<0.0){ // p<0, max
        r=q/p;
        if(r>umin)return false;
        if(r>umax)umax=r;
    }else if(p>0.0){ // p>0, min
        r=q/p;
        if(r<umax)return false;
        if(r<umin)umin=r;
    }else if(q<0.0) return false; // p=0
    return true;
}


void LiangBarsky(float x1, float y1, float x2, float y2){
    /*
     * Liang-Barsky算法
     */
    float umax=0.0, umin=1.0, deltax=x2-x1, deltay=y2-y1;
    float X1, Y1, X2, Y2;
    if(run(-deltax, x1-xwl, umax, umin)){ // 左
        if(run(deltax, xwr-x1, umax, umin)){ // 右
            if(run(-deltay, y1-ywb, umax, umin)){ //下
                if(run(deltay, ywt-y1, umax, umin)){ // 上
                    X2=x1+umin*deltax;
                    Y2=y1+umin*deltay;
                    X1=x1+umax*deltax;
                    Y1=y1+umax*deltay;
                }
                // 画裁剪后的线
                glBegin(GL_LINES);
                glVertex2f(X1, Y1);
                glVertex2f(X2, Y2);
                glEnd();
                // 画被裁剪的线，虚线表示
                glLineStipple(5, 0x5555);
                glEnable(GL_LINE_STIPPLE);
                glBegin(GL_LINES);
                glVertex2f(x1, y1);
                glVertex2f(X1, Y1);
                glEnd();
                glDisable(GL_LINE_STIPPLE);
                glLineStipple(5, 0x5555);
                glEnable(GL_LINE_STIPPLE);
                glBegin(GL_LINES);
                glVertex2f(x2, y2);
                glVertex2f(X2, Y2);
                glEnd();
                glDisable(GL_LINE_STIPPLE);
            }
        }
    }
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    drawxy(); // 画坐标轴和窗口
    glColor3f(0, 0, 0); // black
    LiangBarsky(-1, -2, 3, 3); // 裁剪(-1, -2), (3, 3)在窗口x=2, y=2, x轴, y轴间的线段
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Liang Barsky");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}
