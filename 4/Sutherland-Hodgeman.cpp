//
// Created by Xinyu on 2019/11/14 13:35.
//

#include <gl/glut.h>
#include <vector>

// 多边形坐标
const float pointsx[5]={4,5,8,6,1};
const float pointsy[5]={1,4,1,8,5};
std::vector<float>nowx1;
std::vector<float>nowy1;

// window
const float xwl=2.0f, xwr=6.0f, ywb=2.0f, ywt=6.0f;

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0.0, 8.5, 0.0, 8.5);
}

void draw(){
    /*
     * 画坐标轴和窗口
     */
    glColor3f(0, 0.7, 0.7); // green
    // left
    glBegin(GL_LINES);
    glVertex2f(xwl, ywb);
    glVertex2f(xwl, ywt);
    glEnd();
    // right
    glBegin(GL_LINES);
    glVertex2f(xwr, ywb);
    glVertex2f(xwr, ywt);
    glEnd();
    // top
    glBegin(GL_LINES);
    glVertex2f(xwr, ywt);
    glVertex2f(xwl, ywt);
    glEnd();
    // bottom
    glBegin(GL_LINES);
    glVertex2f(xwr, ywb);
    glVertex2f(xwl, ywb);
    glEnd();

    // 画出多边形轮廓
    glColor3f(1, 1, 1); // white
    for(int i=0;i<=4;i++){
        int next=(i+1)%5;
        glBegin(GL_LINES);
        glVertex2f(pointsx[i], pointsy[i]);
        glVertex2f(pointsx[next], pointsy[next]);
        glEnd();
    }
}

void left(){
    /*
     * 左边界裁剪
     */
    for(int i=0;i<=4;i++){
        bool out1=false,out2=false;
        int next=(i+1)%5;
        if(pointsx[i]<xwl)out1=true;
        if(pointsx[next]<xwl)out2=true;
        if(out1&&out2){ // 都在不可见侧

        }
        else if(!out1&&!out2){ // 都在可见侧
            nowx1.push_back(pointsx[next]);
            nowy1.push_back(pointsy[next]);
        }
        else if(!out1&&out2){ // 可见侧到不可见侧
            float ynow=(pointsy[next]-pointsy[i])/(pointsx[next]-pointsx[i])*(xwl-pointsx[i])+pointsy[i];
            nowx1.push_back(xwl);
            nowy1.push_back(ynow);
        }
        else if(out1&&!out2){ // 不可见侧到可见侧
            float ynow=(pointsy[next]-pointsy[i])/(pointsx[next]-pointsx[i])*(xwl-pointsx[i])+pointsy[i];
            nowx1.push_back(xwl);
            nowy1.push_back(ynow);
            if(xwl!=pointsx[next]&&ynow!=pointsy[next]){ // 避免重复压入
                nowx1.push_back(pointsx[next]);
                nowy1.push_back(pointsy[next]);
            }
        }
    }
}

void bottom(){
    /*
     * 下边界裁剪
     */
    std::vector<float>nowx2(nowx1);
    std::vector<float>nowy2(nowy1);
    nowx1.clear();nowy1.clear();
    for(int i=0;i<nowy2.size();i++){
        bool out1=false,out2=false;
        int next=(i+1)%nowy2.size();
        if(nowy2[i]<ywb)out1=true;
        if(nowy2[next]<ywb)out2=true;
        if(out1&&out2){

        }
        else if(!out1&&!out2){
            nowx1.push_back(nowx2[next]);
            nowy1.push_back(nowy2[next]);
        }
        else if(!out1&&out2){
            float xnow=(nowx2[next]-nowx2[i])/(nowy2[next]-nowy2[i])*(ywb-nowy2[i])+nowx2[i];
            nowx1.push_back(xnow);
            nowy1.push_back(ywb);
        }
        else if(out1&&!out2){
            float xnow=(nowx2[next]-nowx2[i])/(nowy2[next]-nowy2[i])*(ywb-nowy2[i])+nowx2[i];
            nowx1.push_back(xnow);
            nowy1.push_back(ywb);
            if(xnow!=nowx2[next]&&ywb!=nowy2[next]){
                nowx1.push_back(nowx2[next]);
                nowy1.push_back(nowy2[next]);
            }
        }
    }
}

void right(){
    /*
     * 右边界裁剪
     */
    std::vector<float>nowx2(nowx1);
    std::vector<float>nowy2(nowy1);
    nowx1.clear();nowy1.clear();
    for(int i=0;i<nowy2.size();i++){
        bool out1=false,out2=false;
        int next=(i+1)%nowy2.size();
        if(nowx2[i]>xwr)out1=true;
        if(nowx2[next]>xwr)out2=true;
        if(out1&&out2){

        }
        else if(!out1&&!out2){
            nowx1.push_back(nowx2[next]);
            nowy1.push_back(nowy2[next]);
        }
        else if(!out1&&out2){
            float ynow=(nowy2[next]-nowy2[i])/(nowx2[next]-nowx2[i])*(xwr-nowx2[i])+nowy2[i];
            nowx1.push_back(xwr);
            nowy1.push_back(ynow);
        }
        else if(out1&&!out2){
            float ynow=(nowy2[next]-nowy2[i])/(nowx2[next]-nowx2[i])*(xwr-nowx2[i])+nowy2[i];
            nowx1.push_back(xwr);
            nowy1.push_back(ynow);
            if(xwr!=nowx2[next]&&ynow!=nowy2[next]){
                nowx1.push_back(nowx2[next]);
                nowy1.push_back(nowy2[next]);
            }
        }
    }
}

void top(){
    /*
     * 上边界裁剪
     */
    std::vector<float>nowx2(nowx1);
    std::vector<float>nowy2(nowy1);
    nowx1.clear();nowy1.clear();
    for(int i=0;i<nowy2.size();i++){
        bool out1=false,out2=false;
        int next=(i+1)%nowy2.size();
        if(nowy2[i]>ywt)out1=true;
        if(nowy2[next]>ywt)out2=true;
        if(out1&&out2){

        }
        else if(!out1&&!out2){
            nowx1.push_back(nowx2[next]);
            nowy1.push_back(nowy2[next]);
        }
        else if(!out1&&out2){
            float xnow=(nowx2[next]-nowx2[i])/(nowy2[next]-nowy2[i])*(ywt-nowy2[i])+nowx2[i];
            nowx1.push_back(xnow);
            nowy1.push_back(ywt);
        }
        else if(out1&&!out2){
            float xnow=(nowx2[next]-nowx2[i])/(nowy2[next]-nowy2[i])*(ywt-nowy2[i])+nowx2[i];
            nowx1.push_back(xnow);
            nowy1.push_back(ywt);
            if(xnow!=nowx2[next]&&ywt!=nowy2[next]){
                nowx1.push_back(nowx2[next]);
                nowy1.push_back(nowy2[next]);
            }
        }
    }
}

void SutherlandHodgeman(){
    left(); // 剪左边
    bottom(); // 剪下面
    right(); // 剪右边
    top(); // 剪上面
    // 画出裁剪后图形
    glColor3f(0, 0, 0); // black
    for(int i=0;i<nowx1.size();i++){
        int next=(i+1)%nowx1.size();
        glBegin(GL_LINES);
        glVertex2f(nowx1[i], nowy1[i]);
        glVertex2f(nowx1[next], nowy1[next]);
        glEnd();
    }
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    draw(); // 画窗口和多边形
    SutherlandHodgeman(); // 画裁剪后的多边形
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Sutherland Hodgeman");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}
