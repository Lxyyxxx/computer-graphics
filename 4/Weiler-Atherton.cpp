//
// Created by Xinyu on 2019/11/14 13:36.
//

#include <gl/glut.h>
#include <vector>

// 多边形坐标
const float pointsx[5]={4,5,8,6,1};
const float pointsy[5]={1,4,1,8,5};
std::vector<float>nowx1;
std::vector<float>nowy1;

// code
const int lt=0b1001, t=0b1000, rt=0b1010, l=0b0001, c=0b0000, r=0b0010, lb=0b0101, b=0b0100, rb=0b0110;

// window
const float xwl=2.0f, xwr=6.0f, ywb=2.0f, ywt=6.0f;

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0.0, 8.5, 0.0, 8.5);
}

void draw(){
    /*
     * 画坐标轴和窗口
     */
    glColor3f(0, 0.7, 0.7); // green
    // left
    glBegin(GL_LINES);
    glVertex2f(xwl, ywb);
    glVertex2f(xwl, ywt);
    glEnd();
    // right
    glBegin(GL_LINES);
    glVertex2f(xwr, ywb);
    glVertex2f(xwr, ywt);
    glEnd();
    // top
    glBegin(GL_LINES);
    glVertex2f(xwr, ywt);
    glVertex2f(xwl, ywt);
    glEnd();
    // bottom
    glBegin(GL_LINES);
    glVertex2f(xwr, ywb);
    glVertex2f(xwl, ywb);
    glEnd();

    // 画出多边形轮廓
    glColor3f(1, 1, 1); // white
    for(int i=0;i<=4;i++){
        int next=(i+1)%5;
        glBegin(GL_LINES);
        glVertex2f(pointsx[i], pointsy[i]);
        glVertex2f(pointsx[next], pointsy[next]);
        glEnd();
    }
}

int getcode(float x, float y){
    /*
     * 区域编码，用于判断点与窗口的关系
     */
    if(x<xwl&&y>ywt)return lt;
    if(x<xwl&&y<ywb)return lb;
    if(x<xwl)return l;
    if(x>xwr&&y>ywt)return rt;
    if(x>xwr&&y<ywb)return rb;
    if(x>xwr)return r;
    if(y>ywt)return t;
    if(y<ywb)return b;
    return c;
}

void cut(float x1, float y1, float x2, float y2){
    /*
     * 一条线的裁剪
     */
    int code1=getcode(x1, y1), code2=getcode(x2, y2);
    if(code1==0&&code2==0){ // 都在窗口内
        nowx1.push_back(x1);
        nowy1.push_back(y1);
        nowx1.push_back(x2);
        nowy1.push_back(y2);
    }
    else if(code1!=0&&code2!=0&&code1==code2){ // 都在窗口外，且在同一区域

    }
    else if(code1!=0&&code2==0){ // 窗口外到窗口内
        if((code1&0b0001)==0b0001){ // 左
            float ynow=(y2-y1)/(x2-x1)*(xwl-x1)+y1; // 算交点
            nowx1.push_back(xwl); // 压入实交点
            nowy1.push_back(ynow);
            nowx1.push_back(x2); // 压入窗口内的点
            nowy1.push_back(y2);
        }
        else if((code1&0b0010)==0b0010){ // 右
            float ynow=(y2-y1)/(x2-x1)*(xwr-x1)+y1;
            nowx1.push_back(xwr);
            nowy1.push_back(ynow);
            nowx1.push_back(x2);
            nowy1.push_back(y2);
        }
        else if((code1&0b0100)==0b0100){ // 下
            float xnow=(x2-x1)/(y2-y1)*(ywb-y1)+x1;
            nowx1.push_back(xnow);
            nowy1.push_back(ywb);
            nowx1.push_back(x2);
            nowy1.push_back(y2);
        }
        else if((code1&0b1000)==0b1000){ // 上
            float xnow=(x2-x1)/(y2-y1)*(ywt-y1)+x1;
            nowx1.push_back(xnow);
            nowy1.push_back(ywt);
            nowx1.push_back(x2);
            nowy1.push_back(y2);
        }
    }
    else if(code1==0&&code2!=0){ // 窗口内到窗口外
        if((code2&0b0001)==0b0001){ // 左
            float ynow=(y2-y1)/(x2-x1)*(xwl-x1)+y1; // 算交点
            nowx1.push_back(xwl);
            nowy1.push_back(ynow);
        }
        else if((code2&0b0010)==0b0010){ // 右
            float ynow=(y2-y1)/(x2-x1)*(xwr-x1)+y1;
            nowx1.push_back(xwr);
            nowy1.push_back(ynow);
        }
        else if((code2&0b0100)==0b0100){ // 下
            float xnow=(x2-x1)/(y2-y1)*(ywb-y1)+x1;
            nowx1.push_back(xnow);
            nowy1.push_back(ywb);
        }
        else if((code2&0b1000)==0b1000){ // 上
            float xnow=(x2-x1)/(y2-y1)*(ywt-y1)+x1;
            nowx1.push_back(xnow);
            nowy1.push_back(ywt);
        }
    }
    else if(code1!=0&&code2!=0&&code1!=code2) { // 均在窗口外，但不在同一区域，需要判断交点类型
        if((code1&0b0001)==0b0001){ // 左
            float ynow=(y2-y1)/(x2-x1)*(xwl-x1)+y1; // 算交点
            // 压入窗口边界
            if(ynow>ywt){
                nowx1.push_back(xwl);
                nowy1.push_back(ywt);
            }
            else if(ynow<ywb){
                nowx1.push_back(xwl);
                nowy1.push_back(ywb);
            }
            // 压入实交点
            else{
                nowx1.push_back(xwl);
                nowy1.push_back(ynow);
                cut(xwl, ynow, x2, y2);
            }
        }
        else if((code1&0b0010)==0b0010){ // 右
            float ynow=(y2-y1)/(x2-x1)*(xwr-x1)+y1;
            if(ynow>ywt){
                nowx1.push_back(xwr);
                nowy1.push_back(ywt);
            }
            else if(ynow<ywb){
                nowx1.push_back(xwr);
                nowy1.push_back(ywb);
            }
            else{
                nowx1.push_back(xwr);
                nowy1.push_back(ynow);
                cut(xwr, ynow, x2, y2);
            }
        }
        else if((code1&0b0100)==0b0100){ // 下
            float xnow=(x2-x1)/(y2-y1)*(ywb-y1)+x1;
            if(xnow>xwr){
                nowx1.push_back(xwr);
                nowy1.push_back(ywb);
            }
            else if(xnow<xwl){
                nowx1.push_back(xwl);
                nowy1.push_back(ywb);
            }
            else{
                nowx1.push_back(xnow);
                nowy1.push_back(ywb);
                cut(xnow, ywb, x2, y2);
            };
        }
        else if((code1&0b1000)==0b1000){ // 上
            float xnow=(x2-x1)/(y2-y1)*(ywt-y1)+x1;
            if(xnow>xwr){
                nowx1.push_back(xwr);
                nowy1.push_back(ywt);
            }
            else if(xnow<xwl){
                nowx1.push_back(xwl);
                nowy1.push_back(ywt);
            }
            else{
                nowx1.push_back(xnow);
                nowy1.push_back(ywt);
                cut(xnow, ywt, x2, y2);
            }
        }
    }
}

void WeilerAtherton(){
    /*
     * Weiler-Atherton算法
     */
    nowx1.clear();nowy1.clear();
    // 对每条线裁剪
    for(int i=0;i<=4;i++){
        int next=(i+1)%5;
        cut(pointsx[i], pointsy[i], pointsx[next], pointsy[next]);
    }
    // 画出裁剪后图形
    glColor3f(0, 0, 0); // black
    for(int i=0;i<nowx1.size();i++){
        int next=(i+1)%nowx1.size();
        glBegin(GL_LINES);
        glVertex2f(nowx1[i], nowy1[i]);
        glVertex2f(nowx1[next], nowy1[next]);
        glEnd();
    }
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    draw(); // 画窗口和多边形
    WeilerAtherton(); // 画裁剪后的多边形
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Weiler Atherton");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}

