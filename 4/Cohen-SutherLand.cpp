//
// Created by Xinyu on 2019/11/12 10:25.
//

#include <gl/glut.h>

// code
const int lt=0b1001, t=0b1000, rt=0b1010, l=0b0001, c=0b0000, r=0b0010, lb=0b0101, b=0b0100, rb=0b0110;
// window
const float xwl=0.0f, xwr=2.0f, ywb=0.0f, ywt=2.0f;

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(-5.0, 5.0, -5.0, 5.0);
}

void drawxy(){
    /*
     * 画坐标轴和窗口
     */
    glColor3f(0.7, 0.7, 0); // yellow
    // x
    glBegin(GL_LINES);
    glVertex2f(-5, 0);
    glVertex2f(5, 0);
    glEnd();
    // y
    glBegin(GL_LINES);
    glVertex2f(0, -5);
    glVertex2f(0, 5);
    glEnd();

    glColor3f(0, 0.7, 0.7); // green
    // left
    glBegin(GL_LINES);
    glVertex2f(xwl, ywb);
    glVertex2f(xwl, ywt);
    glEnd();
    // right
    glBegin(GL_LINES);
    glVertex2f(xwr, ywb);
    glVertex2f(xwr, ywt);
    glEnd();
    // top
    glBegin(GL_LINES);
    glVertex2f(xwr, ywt);
    glVertex2f(xwl, ywt);
    glEnd();
    // bottom
    glBegin(GL_LINES);
    glVertex2f(xwr, ywb);
    glVertex2f(xwl, ywb);
    glEnd();
}

int getcode(float x, float y){
    /*
     * 得到区域编码
     */
    if(x<xwl&&y>ywt)return lt;
    if(x<xwl&&y<ywb)return lb;
    if(x<xwl)return l;
    if(x>xwr&&y>ywt)return rt;
    if(x>xwr&&y<ywb)return rb;
    if(x>xwr)return r;
    if(y>ywt)return t;
    if(y<ywb)return b;
    return c;
}

void drawstipple(float x1, float y1, float x2, float y2){
    /*
     * 画虚线
     */
    glColor3f(0, 0, 0); // black
    glLineStipple(5, 0x5555);
    glEnable(GL_LINE_STIPPLE);
    glBegin(GL_LINES);
    glVertex2f(x1, y1);
    glVertex2f(x2, y2);
    glEnd();
    glDisable(GL_LINE_STIPPLE);
}

void CohenSutherLand(float x1, float y1, float x2, float y2){
    /*
     * Cohen-SutherLand算法
     */
    int code1=getcode(x1, y1), code2=getcode(x2, y2);
    if((code1|code2)==0){ // 均在窗口内
        // 画实线
        glColor3f(0, 0, 0); // black
        glBegin(GL_LINES);
        glVertex2f(x1, y1);
        glVertex2f(x2, y2);
        glEnd();
    }
    else if((code1&code2)!=0){ // 均在窗口同一外侧

    }
    else{
        if(code1==0){ // p1在窗口外，交换
            float tmp=x1;x1=x2;x2=tmp;
            tmp=y1;y1=y2;y2=tmp;
            tmp=code1;code1=code2;code2=tmp;
        }
        if((code1&0b0001)==0b0001){ // 左
            float ynow=(y2-y1)/(x2-x1)*(xwl-x1)+y1; // 算交点
            drawstipple(x1, y1, xwl, ynow); // 去掉的画成虚线
            CohenSutherLand(xwl, ynow, x2, y2); // 继续算
        }
        else if((code1&0b0010)==0b0010){ // 右
            float ynow=(y2-y1)/(x2-x1)*(xwr-x1)+y1;
            drawstipple(x1, y1, xwr, ynow);
            CohenSutherLand(xwr, ynow, x2, y2);
        }
        else if((code1&0b0100)==0b0100){ // 下
            float xnow=(x2-x1)/(y2-y1)*(ywb-y1)+x1;
            drawstipple(x1, y1, xnow, ywb);
            CohenSutherLand(xnow, ywb, x2, y2);
        }
        else if((code1&0b1000)==0b1000){ // 上
            float xnow=(x2-x1)/(y2-y1)*(ywt-y1)+x1;
            drawstipple(x1, y1, xnow, ywt);
            CohenSutherLand(xnow, ywt, x2, y2);
        }
    }
}


void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    drawxy(); // 画坐标轴和窗口
    CohenSutherLand(-1, -2, 3, 3); // 裁剪(-1, -2), (3, 3)在窗口x=2, y=2, x轴, y轴间的线段
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Cohen SutherLand");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}
