//
// Created by Xinyu on 2019/12/7 14:39.
//

#include <gl/glut.h>
#include <cmath>
#include <cstdlib>
#include <ctime>

const int number=15; // 产生雪花的个数
const float height=15.0f,width=15.0f;

void Initial(){
    glEnable(GL_LIGHTING); // 打开光照和深度测试
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0f, 0.0f, 0.0f,0.0f); // 背景为黑色
}

void ChangeSize(int w, int h) {
    if(h == 0)  h = 1;
    glViewport((w-h)/2, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if(w <= h)
        gluOrtho2D(0,width,0,height*h/w);
    else
        gluOrtho2D(0,width*w/h,0,height);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

struct Point{
    GLfloat x;
    GLfloat y;
};

void Line(Point Point1,Point Point2){
    /*
     * 画直线
     */
    glBegin(GL_LINES);
    glVertex2d(Point1.x,Point1.y);
    glVertex2d(Point2.x,Point2.y);
    glEnd();
}

void Kock(Point Point1,Point Point2,int n){
    /*
     * 迭代Kock雪花
     */
    Point p1,p2,p3,p4,p5;
    p1.x=Point1.x;p1.y=Point1.y;
    p5.x=Point2.x;p5.y=Point2.y;
    if(n==0)exit(0);
    if(n==1)Line(p1,p5);
    if(n>1){
        p2.x=p1.x+(p5.x-p1.x)/3;
        p2.y=p1.y+(p5.y-p1.y)/3;
        p3.x=(p5.x+p1.x)/2-(p5.y-p1.y)*sqrt(3)/6;
        p3.y=(p5.y+p1.y)/2+(p5.x-p1.x)*sqrt(3)/6;
        p4.x=p1.x+2*(p5.x-p1.x)/3;
        p4.y=p1.y+2*(p5.y-p1.y)/3;
        Kock(p1,p2,n-1);
        Kock(p2,p3,n-1);
        Kock(p3,p4,n-1);
        Kock(p4,p5,n-1);
    }
}

void KockN(int n){
    /*
     * 产生n次迭代的Kock雪花
     */
    Point p1,p2,p3;
    p1.x=-sqrt(3);p1.y=-1;
    p2.x=0;p2.y=2;
    p3.x=sqrt(3);p3.y=-1;
    Kock(p1,p2,n);
    Kock(p2,p3,n);
    Kock(p3,p1,n);
}

float random(int a,int b){
    /*
     * 产生a~b之间的随机数
     */
    if(a==0&&b==1)return float(rand())/float(RAND_MAX);
    return (rand()%(b-a))+a;
}

void Display(){
    /*
     * 显示函数
     */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
    for(int i=0;i<number;i++) { // 循环产生n个随机属性的雪花
        glPushMatrix();
        float x=random(0,1),y=random(0,1),z=random(0,1); // 产生3个随机数
        glTranslatef(x*width, y*height, 0); // 平移
        glScalef(z,z,1.0f); // 缩放雪花大小
        glRotatef(z*30,0,0,1); // 旋转
        GLfloat mat_diffuse[]= { x, y, z, 1.0f }; // 添加材质属性
        glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
        KockN(5); // 画雪花
        glPopMatrix();
    }
    glPopMatrix();
    glutSwapBuffers();
}

void Timer(int value){
    /*
     * 用于产生动画
     */
    glutPostRedisplay();
    glutTimerFunc(80,Timer,1);
}

int main(int argc,char**argv){
    srand((unsigned)time(NULL)); // 用于产生不同的随机数
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
    glutInitWindowPosition(100,100);
    glutInitWindowSize(500,500);
    glutCreateWindow("彩色的雪花");
    glutReshapeFunc(ChangeSize);
    glutDisplayFunc(Display);
    glutTimerFunc(80,Timer,1); // 产生动画
    Initial();
    glutMainLoop();
    return 0;
}
