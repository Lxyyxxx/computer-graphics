# 计算机图形学的实验

使用OpenGL + C++98 完成

1
===
1. 球体
2. 自由落体的球体动画
3. 沿直线前进的球体动画
4. 绘制实线、虚线和点划线
5. 用黑白相间的棋盘图案填充多边形

2
===
1. Bresenham生成直线
2. 中点Bresenham生成直线
3. 中点Bresenham生成圆
4. 中点Bresenham生成椭圆
5. 分别用点和折线模式实现正弦和余弦曲线的绘制
6. 在屏幕上输出“OpenGL”字样

3
===
1. 边标志填充

4
===
 1. Cohen SutherLand裁剪直线
 2. Liang Barsky裁剪直线
 3. Sutherland Hodgeman裁剪多边形
 4. Weiler Atherton裁剪多边形

5
===
1. 交互式正弦曲线的变换

6
===
1. 综合实验，彩色的Kock雪花动画

