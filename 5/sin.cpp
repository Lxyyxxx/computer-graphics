//
// Created by Xinyu on 2019/11/30 18:12.
//

#include <gl/glut.h>
#include <cmath>

const double pi=3.1415926;
const float width=10,height=10;
float xTra=0.0,yTra=0.0,zRot=0.0,xSca=1.0,ySca=1.0;

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
}

void ChangeSize(int w, int h){
    glViewport((w-h)/2.0,0,h,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-width, width, -height, height);
}

void DrawXY(){
    glColor3f(0.75, 0.75, 0.75); // grey
    // x
    glBegin(GL_LINES);
    glVertex2f(-width, 0);
    glVertex2f(width, 0);
    glEnd();
    // y
    glBegin(GL_LINES);
    glVertex2f(0, -height);
    glVertex2f(0, height);
    glEnd();
}

void LineSin(){
    glClear(GL_COLOR_BUFFER_BIT);
    glPushMatrix();
    glTranslatef(xTra,yTra,0);
    glRotatef(zRot,0,0,1); // 绕z轴旋转，正方形保证旋转不会变形
    glScalef(xSca,ySca,1);
    DrawXY();
    glColor3f(0.5f, 0.0f, 0.0f); // red
    glBegin(GL_LINE_STRIP);
    double x;
    for(x=-2*pi; x<=2*pi+0.1; x+=0.1)
        glVertex2f(x, sin(x));
    glEnd();
    glPopMatrix();
    glutSwapBuffers();
}

void Menu(int value){
    switch(value){
        case 1: // 左移
            xTra-=0.1;
            break;
        case 2: // 右移
            xTra+=0.1;
            break;
        case 3: // 上移
            yTra+=0.1;
            break;
        case 4: // 下移
            yTra-=0.1;
            break;
        case 5: // 左旋
            zRot+=5;
            break;
        case 6: // 右旋
            zRot-=5;
            break;
        case 7: // 放大
            xSca*=1.1;ySca*=1.1;
            break;
        case 8: // 缩小
            xSca*=0.9;ySca*=0.9;
            break;
        case 9: // 关于x轴对称
            ySca=-ySca;
            break;
        case 10: // 关于y轴对称
            xSca=-xSca;
            break;
        default:
            break;
    }
    glutPostRedisplay();
}

void Keys(unsigned char key, int x,int y){
    switch(key){
        case 'a': // 左移
            Menu(1);
            break;
        case 'd': // 右移
            Menu(2);
            break;
        case 'w': // 上移
            Menu(3);
            break;
        case 's': // 下移
            Menu(4);
            break;
        case 'q': // 左旋
            Menu(5);
            break;
        case 'e': // 右旋
            Menu(6);
            break;
        case 'r': // 放大
            Menu(7);
            break;
        case 'f': // 缩小
            Menu(8);
            break;
        case 'z': // 关于x轴对称
            Menu(9);
            break;
        case 'x': // 关于y轴对称
            Menu(10);
            break;
        default:
            break;
    }
    glutPostRedisplay();
}

void ShowMenu(){
    glutCreateMenu(Menu);
    glutAddMenuEntry("左移(a)",1);
    glutAddMenuEntry("右移(d)",2);
    glutAddMenuEntry("上移(w)",3);
    glutAddMenuEntry("下移(s)",4);
    glutAddMenuEntry("绕z轴左旋(q)",5);
    glutAddMenuEntry("绕z轴右旋(e)",6);
    glutAddMenuEntry("放大(r)",7);
    glutAddMenuEntry("缩小(f)",8);
    glutAddMenuEntry("关于x轴对称(z)",9);
    glutAddMenuEntry("关于y轴对称(x)",10);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Sin");
    glutDisplayFunc(LineSin);
    glutReshapeFunc(ChangeSize);
    glutKeyboardFunc(Keys);
    ShowMenu();
    Initial();
    glutMainLoop();
    return 0;
}