//
// Created by Xinyu on 2019/11/14 12:35.
// 边标志填充算法
//

#include <gl/glut.h>

// 多边形坐标
const int pointsx[7]={3,6,8,12,7,3,1};
const int pointsy[7]={1,5,1,9,8,12,7};
bool flags[14][14]={false};

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0, 13.0, 0.0, 13.0);
}

void run1(int dx, int dy, int x, int y, int end){
    /*
     * 0<=k<=1
     */
    glColor3f(0.0f, 0.0f, 1.0f); // blue
    int d=dx-2*dy; // 初始值
    while(x<=end){
        flags[x][y]=!flags[x][y];
        x++;
        if(d<0){ // d<0, 取上面点
            y++;
            d+=(2*dx-2*dy);
        }
        else d+=(-2*dy); // d>=0, 取下面点
    }
}

void run2(int dx, int dy, int x, int y, int end){
    /*
     * -1<=k<0
     */
    glColor3f(1.0f, 0.0f, 0.0f); // red
    int d=-dx-2*dy; // 初始值
    while(x<=end){
        flags[x][y]=!flags[x][y];
        x++;
        if(d>=0){ // d>=0, 取下面点
            y--;
            d+=(-2*dx-2*dy);
        }
        else  d+=(-2*dy);// d<0, 取上面点
    }
}

void run3(int dx, int dy, int x, int y, int end){
    /*
     * k>1
     */
    glColor3f(0.0f, 0.0f, 0.0f); // black
    int d=2*dx-dy; // 初始值
    while(y<=end){
        flags[x][y]=!flags[x][y];
        y++;
        if(d>=0){ // d>=0, 取右边点
            x++;
            d+=(2*dx-2*dy);
        }
        else  d+=(2*dx);// d<0, 取左边点
    }
}

void run4(int dx, int dy, int x, int y, int end){
    /*
     * k<-1
     */
    int d=-2*dx-dy; // 初始值
    while(y>=end){
        flags[x][y]=!flags[x][y];
        y--;
        if(d<0){ // d<0, 取右边点
            x++;
            d+=(-2*dx-2*dy);
        }
        else  d+=(-2*dx); // d>=0, 取左边点
    }
}

void MidBresenhamLine(int x0, int y0, int x1, int y1){
    /*
     * 中点Bresenham生成直线，用于标志
     */
    // 使dx符号固定，把小的x0点当作p0
    if(x0>x1){
        int tmp;
        tmp=x0; x0=x1; x1=tmp;
        tmp=y0; y0=y1; y1=tmp;
    }
    int dx, dy;
    dx=x1-x0; dy=y1-y0;
    // 0<=k<=1
    if(dy<=dx && dy>=0) run1(dx, dy, x0, y0, x1);
    // -1<=k<0
    else if(dy>=-dx && dy<0) run2(dx, dy, x0, y0, x1);
    // k>1
    else if(dy>dx) run3(dx, dy, x0, y0, y1);
    // k<-1
    else if(dy<-dx) run4(dx, dy, x0, y0, y1);
}

void mark(){
    /*
     * 标志
     */
    for(int i=0;i<=6;i++){
        int next=(i+1)%7, pre=(i+7-1)%7;
        MidBresenhamLine(pointsx[i], pointsy[i], pointsx[next], pointsy[next]);
        // 特判交点
        if((pointsy[pre]<pointsy[i]&&pointsy[next]>pointsy[i])||(pointsy[pre]>pointsy[i]&&pointsy[next]<pointsy[i])){
            flags[pointsx[i]][pointsy[i]]=!flags[pointsx[i]][pointsy[i]];
        }
        // 画出多边形轮廓
        glColor3f(1, 1, 1); // white
        glBegin(GL_LINES);
        glVertex2f(pointsx[i], pointsy[i]);
        glVertex2f(pointsx[next], pointsy[next]);
        glEnd();
    }
}

void fill(){
    /*
     * 填充
     */
    for(int j=13;j>=0;j--){
        bool inside=false;
        for(int i=0;i<=13;i++){
            if(flags[i][j])inside=!inside;
            if(inside)glVertex2i(i, j); // 画点
        }
    }
}

void EdgeMarkFill(){
    /*
     * 边标志填充
     */
    mark(); // 标志
    glColor3f(0.0f, 0.0f, 0.0f); // black
    glPointSize(3.0f);
    glBegin(GL_POINTS); // 开始画点模式
    fill(); // 填充
    glEnd(); // 画点结束
}


void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    EdgeMarkFill(); // 边标志填充
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("EdgeMarkFill");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}


