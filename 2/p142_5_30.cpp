//
// Created by Xinyu on 2019/10/15 17:03.
// 课本P142 5.30
// 利用OpenGL在屏幕上输出“OpenGL”字样
//

#include <GL/glut.h>

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0.0, 10.0, 0.0, 10.0);
}

void OpenGL(){
    /*
     * 在屏幕上输出OpenGL
     */
    char c[7]="OpenGL";
    // 位图字符
    glColor3f(0.0f, 0.0f, 0.5f); // blue
    glRasterPos2f(4.0, 6.0); // 显示字体时设置字符的起始位置
    for(char i:c){
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, i); // i为ASCII码值(int)
    }
    // 矢量字符
    glColor3f(0.5f, 0.0f, 0.0f); // red
    glTranslatef(3.0f, 4.0f, 0.0f); // 显示字体时设置字符的起始位置
    glScalef(1/150.0, 1/150.0, 1/150.0); // 缩小字符使可以显示
    for(char i:c){
        glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, i); // i为ASCII码值(int)
    }

}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    OpenGL();
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Print OpenGL");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}
