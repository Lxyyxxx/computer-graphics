//
// Created by Xinyu on 2019/10/15 13:28.
// Bre生成直线
//

#include <GL/glut.h>

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0.0, 12.0, 0.0, 12.0);
}

void run1(int dx, int dy, int x, int y, int end){
    /*
     * 0<=k<=1
     */
    glColor3f(0.0f, 0.0f, 1.0f); // blue
    int d=-dx; // 初始值
    while(x<=end){
        glVertex2i(x, y); // 画点
        d+=2*dy; // 更新d
        if(d>0){ // d>0，选择上面点
            y++;
            d+=(-2*dx);
        }
        x++;
    }
}

void run2(int dx, int dy, int x, int y, int end){
    /*
     * -1<=k<0
     */
    glColor3f(1.0f, 0.0f, 0.0f); // red
    int d=-dx; // 初始值
    while(x<=end){
        glVertex2i(x, y); // 画点
        d+=(-2*dy); // 更新d
        if(d>0){ // d>0，选择下面点
            y--;
            d+=(-2*dx);
        }
        x++;
    }
}

void run3(int dx, int dy, int x, int y, int end){
    /*
     * k>1
     */
    glColor3f(0.0f, 0.0f, 0.0f); // black
    int d=-dy; // 初始值
    while(y<=end){
        glVertex2i(x, y); // 画点
        d+=(2*dx); // 更新d
        if(d>0){ // d>0，选择右边点
            x++;
            d+=(-2*dy);
        }
        y++;
    }
}

void run4(int dx, int dy, int x, int y, int end){
    /*
     * k<-1
     */
    glColor3f(1.0f, 1.0f, 1.0f); // white
    int d=dy; // 初始值
    while(y>=end){
        glVertex2i(x, y); // 画点
        d+=(2*dx); // 更新d
        if(d>0){ // d>0，选择右边点
            x++;
            d+=(2*dy);
        }
        y--;
    }
}

void BresenhamLine(int x0, int y0, int x1, int y1){
    /*
     * Bresenham生成直线
     */
    glPointSize(3.0f);
    glBegin(GL_POINTS); // 开始画点模式
    // 使dx符号固定，把小的x0点当作p0
    if(x0>x1){
        int tmp;
        tmp=x0; x0=x1; x1=tmp;
        tmp=y0; y0=y1; y1=tmp;
    }
    int dx, dy;
    dx=x1-x0; dy=y1-y0;
    // 0<=k<=1
    if(dy<=dx && dy>=0) run1(dx, dy, x0, y0, x1);
    // -1<=k<0
    else if(dy>=-dx && dy<0) run2(dx, dy, x0, y0, x1);
    // k>1
    else if(dy>dx) run3(dx, dy, x0, y0, y1);
    // k<-1
    else if(dy<-dx) run4(dx, dy, x0, y0, y1);
    glEnd(); // 画点结束
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    // 画(0,0)到(11,8)的直线
    BresenhamLine(0, 0, 11, 8);
    // BresenhamLine(11, 0, 0, 8);
    // BresenhamLine(0, 0, 8, 11);
    // BresenhamLine(8, 0, 0, 11);
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Bresenham Line");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}