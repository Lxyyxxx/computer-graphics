//
// Created by Xinyu on 2019/10/15 17:28.
// 课本P142 5.29
// 利用OpenGL，分别用点和折线模式实现正弦和余弦曲线的绘制
//

#include <GL/glut.h>
#include <cmath>

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0.0, 50.0, -20.0, 110.0);
}

void PointSin(){
    /*
     * 点模式绘制正弦曲线
     */
    glColor3f(0.5f, 0.0f, 0.0f); // red
    glPointSize(3.0f); // 点模式
    glBegin(GL_POINTS);
    int x, y;
    for(x=0; x<=50; x++){
        y=8*sin(x); // sin
        glVertex2i(x, y);
    }
    glEnd();
}

void LineSin(){
    /*
     * 折线模式绘制正弦曲线
     */
    glColor3f(0.5f, 0.0f, 0.0f); // red
    glPointSize(3.0f);
    glBegin(GL_LINE_STRIP); // 折线模式
    int x, y;
    for(x=0; x<=50; x++){
        y=8*sin(x)+60; // sin
        glVertex2i(x, y);
    }
    glEnd();
}

void PointCos(){
    /*
     * 点模式绘制余弦曲线
     */
    glColor3f(0.0f, 0.0f, 0.5f); // blue
    glPointSize(3.0f); // 点模式
    glBegin(GL_POINTS);
    int x, y;
    for(x=0; x<=50; x++){
        y=8*cos(x)+30; // cos
        glVertex2i(x, y);
    }
    glEnd();
}

void LineCos(){
    /*
     * 折线模式绘制余弦曲线
     */
    glColor3f(0.0f, 0.0f, 0.5f); // blue
    glPointSize(3.0f);
    glBegin(GL_LINE_STRIP); // 折线模式
    int x, y;
    for(x=0; x<=50; x++){
        y=8*cos(x)+90; // cos
        glVertex2i(x, y);
    }
    glEnd();
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    PointSin(); // 点模式绘制正弦曲线
    LineSin(); // 折线模式绘制正弦曲线
    PointCos(); // 点模式绘制余弦曲线
    LineCos(); // 折线模式绘制余弦曲线
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Print Sin & Cos");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}
