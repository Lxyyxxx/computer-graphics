//
// Created by Xinyu on 2019/10/15 13:53.
// Bre算法生成椭圆
//

#include <GL/glut.h>

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(-12.0, 12.0, -9.0, 9.0);
}

void point4(int x, int y){
    /*
     * 画4个点
     */
    glVertex2i(x, y); glVertex2i(-x, -y); glVertex2i(-x, y); glVertex2i(x, -y);
}

void MidBresenhamEllipse(int a, int b){
    /*
     * Bre生成椭圆
     */
    glColor3f(0.0f, 0.0f, 0.0f); // black
    glPointSize(3.0f);
    glBegin(GL_POINTS); // 开始画点模式
    int x, y;
    float d1, d2;
    x=0; y=b;
    // 上半部分，x自增
    d1=b*b+a*a*(-b+0.25f); // 初始值
    point4(x, y);
    while(b*b*(x+1)<a*a*(y-0.5)){
        if(d1<=0) d1+=(b*b*(2*x+3)); // d1<=0，选上面的点
        else{ // d1>0，选下面的点
            d1+=(b*b*(2*x+3)+a*a*(-2*y+2));
            y--;
        }
        x++;
        point4(x, y);
    }
    // 下半部分，y自减
    d2=b*b*(x+0.5)*(x+0.5)+a*a*(y-1)*(y-1)-a*a*b*b; // 初始值
    while(y>0){
        if(d2>0) d2+=(a*a*(-2*y+3)); // d2>0，选左边的点
        else{ // d2<=0，选右边的点
            d2+=(b*b*(2*x+2)+a*a*(-2*y+3));
            x++;
        }
        y--;
        point4(x, y);
    }
    glEnd(); // 画点结束
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    // 画a=10,b=8的椭圆
    MidBresenhamEllipse(10, 8);
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Mid Bresenham Ellipse");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}