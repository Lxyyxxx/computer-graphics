//
// Created by Xinyu on 2019/10/15 13:47.
// Bre算法生成圆
//

#include <GL/glut.h>

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(-12.0, 12.0, -9.0, 9.0);
}

void point8(int x, int y){
    /*
     * 画8个点，也就是八分法画圆
     */
    glVertex2i(x, y); glVertex2i(-x, -y); glVertex2i(-x, y); glVertex2i(x, -y);
    glVertex2i(y, x); glVertex2i(-y, -x); glVertex2i(y, -x); glVertex2i(-y, x);
}

void MidBresenhamCircle(int r){
    /*
     * Bre算法生成圆
     */
    glColor3f(0.0f, 0.0f, 0.0f); // black
    glPointSize(3.0f);
    glBegin(GL_POINTS); // 开始画点模式
    int x, y, d;
    x=0; y=r; d=1-r;
    while(x<=y){
        point8(x, y);
        if(d<0) d+=(2*x+3);
        else{
            d+=(2*(x-y)+5);
            y--;
        }
        x++;
    }
    glEnd(); // 画点结束
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    // 画r=8的圆
    MidBresenhamCircle(8);
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Mid Bresenham Circle");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}
