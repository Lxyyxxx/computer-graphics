//
// Created by Xinyu on 2019/10/15 13:27.
// 中点Bre生成直线
//

#include <GL/glut.h>

void Initial() {
    glClearColor(1.0f, 0.8f, 0.8f, 1.0f); // pink
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0.0, 12.0, 0.0, 12.0);
}

void run1(int dx, int dy, int x, int y, int end){
    /*
     * 0<=k<=1
     */
    glColor3f(0.0f, 0.0f, 1.0f); // blue
    int d=dx-2*dy; // 初始值
    while(x<=end){
        glVertex2i(x, y); // 画点
        x++;
        if(d<0){ // d<0, 取上面点
            y++;
            d+=(2*dx-2*dy);
        }
        else d+=(-2*dy); // d>=0, 取下面点
    }
}

void run2(int dx, int dy, int x, int y, int end){
    /*
     * -1<=k<0
     */
    glColor3f(1.0f, 0.0f, 0.0f); // red
    int d=-dx-2*dy; // 初始值
    while(x<=end){
        glVertex2i(x, y); // 画点
        x++;
        if(d>=0){ // d>=0, 取下面点
            y--;
            d+=(-2*dx-2*dy);
        }
        else  d+=(-2*dy);// d<0, 取上面点
    }
}

void run3(int dx, int dy, int x, int y, int end){
    /*
     * k>1
     */
    glColor3f(0.0f, 0.0f, 0.0f); // black
    int d=2*dx-dy; // 初始值
    while(y<=end){
        glVertex2i(x, y); // 画点
        y++;
        if(d>=0){ // d>=0, 取右边点
            x++;
            d+=(2*dx-2*dy);
        }
        else  d+=(2*dx);// d<0, 取左边点
    }
}

void run4(int dx, int dy, int x, int y, int end){
    /*
     * k<-1
     */
    glColor3f(1.0f, 1.0f, 1.0f); // white
    int d=-2*dx-dy; // 初始值
    while(y>=end){
        glVertex2i(x, y); // 画点
        y--;
        if(d<0){ // d<0, 取右边点
            x++;
            d+=(-2*dx-2*dy);
        }
        else  d+=(-2*dx); // d>=0, 取左边点
    }
}

void MidBresenhamLine(int x0, int y0, int x1, int y1){
    /*
     * 中点Bresenham生成直线
     */
    glPointSize(3.0f);
    glBegin(GL_POINTS); // 开始画点模式
    // 使dx符号固定，把小的x0点当作p0
    if(x0>x1){
        int tmp;
        tmp=x0; x0=x1; x1=tmp;
        tmp=y0; y0=y1; y1=tmp;
    }
    int dx, dy;
    dx=x1-x0; dy=y1-y0;
    // 0<=k<=1
    if(dy<=dx && dy>=0) run1(dx, dy, x0, y0, x1);
    // -1<=k<0
    else if(dy>=-dx && dy<0) run2(dx, dy, x0, y0, x1);
    // k>1
    else if(dy>dx) run3(dx, dy, x0, y0, y1);
    // k<-1
    else if(dy<-dx) run4(dx, dy, x0, y0, y1);
    glEnd(); // 画点结束
}

void Display() {
    glClear(GL_COLOR_BUFFER_BIT);
    // 画(0,0)到(11,8)的直线
    MidBresenhamLine(0, 0, 11, 8);
    // MidBresenhamLine(11, 0, 0, 8);
    // MidBresenhamLine(0, 0, 8, 11);
    // MidBresenhamLine(8, 0, 0, 11);
    glFlush();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400, 300);
    glutInitWindowPosition(100, 120);
    glutCreateWindow("Mid Bresenham Line");
    glutDisplayFunc(Display);
    Initial();
    glutMainLoop();
    return 0;
}
